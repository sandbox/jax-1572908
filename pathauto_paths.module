<?php

/**
 * Implements hook_menu().
 */
function pathauto_paths_menu() {
  $items['admin/config/search/path/path'] = array(
    'title' => 'Paths',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pathauto_paths_settings_form'),
    'access arguments' => array('administer pathauto'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 25,
    'file' => 'pathauto_paths.admin.inc',
  );

  $items['admin/config/search/path/path/%pathauto_paths'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pathauto_paths_settings_form', 5),
    'access arguments' => array('administer pathauto'),
    'file' => 'pathauto_paths.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/config/search/path/path/%pathauto_paths/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pathauto_paths_delete_confirm_form', 5),
    'access arguments' => array('administer pathauto'),
    'file' => 'pathauto_paths.admin.inc',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_entity_insert().
 */
function pathauto_paths_entity_insert($entity, $type) {
  pathauto_paths_entity_update($entity, $type, 'insert');
}

/**
 * Implements hook_entity_update().
 */
function pathauto_paths_entity_update($entity, $type, $op = 'update') {
  if ($type == 'i18n_translation') {
    // No clue when this happens.
    var_dump('CLUE!', $type, $entity);
    aaah();
    return;
  }
  $info = entity_get_info($type);
  // Vocabularies don't have a bundle and should be skipped.
  if (empty($info['entity keys']['bundle'])) {
    return;
  }
  // Taxonomy terms that are localized should sheck for translations.
  if($type == "taxonomy_term" && module_exists('i18n_taxnomy') && i18n_taxonomy_vocabulary_mode($entity->vid, I18N_MODE_LOCALIZE)) {
    $object = i18n_object($type, $entity);
    $strings = $object->get_strings(array('empty' => TRUE));
    $source_language = variable_get_value('i18n_string_source_language');
    $translated = false;
    foreach (language_list() as $langcode => $language) {
      foreach ($strings as $i18nstring) {
        if($i18nstring->get_translation($langcode)) {
          $translated = true;
          pathauto_paths_process_patterns($entity, $info, $type, $langcode, $op, $i18nstring);
        }
      }
    }
    pathauto_paths_process_patterns($entity, $info, $type, $source_language, $op);
  }
  else {
    $language = isset($entity->language) ? $entity->language : LANGUAGE_NONE;
    pathauto_paths_process_patterns($entity, $info, $type, $language, $op);
  }
}

/**
 * Get all paterns from the database and make an alias from each patern.
 */
function pathauto_paths_process_patterns($entity, $info, $type, $language, $op, $string = null) {
  $name = $entity->name;
  $result = db_select('pathauto_paths', 'pp')
    ->fields('pp')
    ->condition('entity_type', $type)
    ->condition('bundle', $entity->{$info['entity keys']['bundle']})
    ->condition('language', array('', $language))
    ->execute();
  $rows = $result->rowCount();
  module_load_include('inc', 'pathauto');
  if($string) {
    $entity->name = $string->get_translation($language);
  }
  $data = array(
    $type => $entity,
  );
  $entity_id = $entity->{$info['entity keys']['id']};
  foreach ($result as $row) {
    // Check if the language in the row isn't empty,
    // otherwise chech if the row with the empty language is the only row,
    // then make a new alias
    if($row->language || $rows == 1) {
      $source = str_replace('%', $entity_id, $row->router_path);
      pathauto_paths_create_alias($op, $row->pattern, $source, $data, $language);
    }
  }
  $entity->name = $name;
}

/**
 * This is almost identical to pathauto_create_alias() except that a pattern
 * can be passed and no hooks are called.
 * 
 * @see pathauto_create_alias() 7.x-1.0
 */
function pathauto_paths_create_alias($op, $pattern, $source, $data, $language = LANGUAGE_NONE) {
  // Special handling when updating an item which is already aliased.
  $existing_alias = NULL;
  if ($op == 'update') {
    if ($existing_alias = _pathauto_existing_alias_data($source, $language)) {
      switch (variable_get('pathauto_update_action', PATHAUTO_UPDATE_ACTION_DELETE)) {
        case PATHAUTO_UPDATE_ACTION_NO_NEW:
          // If an alias already exists, and the update action is set to do nothing,
          // then gosh-darn it, do nothing.
          return '';
      }
    }
  }
  // Replace any tokens in the pattern. Uses callback option to clean replacements. No sanitization.
  $alias = token_replace($pattern, $data, array(
    'sanitize' => FALSE,
    'clear' => TRUE,
    'callback' => 'pathauto_clean_token_values',
    'language' => (object) array('language' => $language),
    'pathauto' => TRUE,
  ));
  // Check if the token replacement has not actually replaced any values. If
  // that is the case, then stop because we should not generate an alias.
  // @see token_scan()
  $pattern_tokens_removed = preg_replace('/\[[^\s\]:]*:[^\s\]]*\]/', '', $pattern);
  if ($alias === $pattern_tokens_removed) {
    return '';
  }
  $alias = pathauto_clean_alias($alias);
  // If we have arrived at an empty string, discontinue.
  if (!drupal_strlen($alias)) {
    return '';
  }
  // If the alias already exists, generate a new, hopefully unique, variant.
  $original_alias = $alias;
  pathauto_alias_uniquify($alias, $source, $language);
  if ($original_alias != $alias) {
    // Alert the user why this happened.
    _pathauto_verbose(t('The automatically generated alias %original_alias conflicted with an existing alias. Alias changed to %alias.', array(
      '%original_alias' => $original_alias,
      '%alias' => $alias,
    )), $op);
  }
  // Return the generated alias if requested.
  if ($op == 'return') {
    return $alias;
  }
  // Build the new path alias array and send it off to be created.
  $path = array(
    'source' => $source,
    'alias' => $alias,
    'language' => $language,
  );
  $path = _pathauto_set_alias($path, $existing_alias, $op);
  return $path;
}

function pathauto_paths_load($id) {
  $record = db_select('pathauto_paths', 'pp')
    ->fields('pp')
    ->condition('id', $id)
    ->execute()
    ->fetchAssoc();
  return $record;
}

/**
 * Implements hook_entity_delete().
 */
function pathauto_paths_entity_delete($entity, $type) {
  $info = entity_get_info($type);
  if (empty($info['entity keys']['bundle'])) {
    return;
  }
  $entity_id = $entity->{$info['entity keys']['id']};
  $patterns = db_select('pathauto_paths', 'pp')
    ->fields('pp')
    ->condition('entity_type', $type)
    ->condition('bundle', $entity->{$info['entity keys']['bundle']})
    ->execute()
    ->fetchAll();
  if (empty($patterns)) {
    return;
  }
  $sources = array();
  foreach ($patterns as $pattern) {
    $sources[] = str_replace('%', $entity_id, $pattern->router_path);
  }
  if ($sources) {
    db_delete('url_alias')
      ->condition('source', $sources)
      ->execute();
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function pathauto_paths_form_i18n_string_translate_page_form_alter(&$form, &$form_state) {
  $form['#submit'][] = 'pathauto_paths_i18n_string_translate_page_form_submit';
}

/**
 * handels form_i18n_string_translate_page_form submit
 */
function pathauto_paths_i18n_string_translate_page_form_submit ($form, &$form_state) {
  foreach ($form_state['build_info']['args'][0] as $string) {
    if($string->get_title() == t('Name')) {
      $type = $string->textgroup . '_' . $string->type;
      $entity = entity_load($type, array($string->objectid));
      pathauto_paths_entity_update($entity[$string->objectid], $type);
    }
  }
}