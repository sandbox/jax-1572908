<?php

/**
 * Pathauto paths settings form callback.
 */
function pathauto_paths_settings_form($form, &$form_state, $id = 0) {
  $record = array();
  if ($id) {
    $record = db_select('pathauto_paths', 'pp')
      ->fields('pp')
      ->condition('id', $id)
      ->execute()
      ->fetchAssoc();
  }
  if (empty($record)) {
    $record = array(
      'id' => 0,
      'router_path' => '',
      'entity_type' => NULL,
      'bundle' => NULL,
      'pattern' => NULL,
    );
  }
  $form_state['pattern_id'] = $id;
  $form['router_path'] = array(
    '#type' => 'textfield',
    '#title' => 'Router path',
    '#required' => TRUE,
    '#default_value' => $record['router_path'],
  );
  $languages = language_list();
  $language_options = array();
  $language_options[''] = 'All languages';
  $language_options['und'] = 'Language neutral';
  foreach ($languages as $key => $value) {
    $language_options[$key] = $value->name;
  }
  $form['language'] = array(
    '#type' => 'select',
    '#title' => 'Language',
    '#options' => $language_options,
    '#required' => FALSE,
    '#default_value' => $record['id'] ?
      $record['language'] :
      '',
  );
  $languages = language_list();
  $language_options = array();
  $language_options[''] = 'Default';
  $language_options['und'] = 'Language neutral';
  foreach ($languages as $key => $value) {
    $language_options[$key] = $value->name;
  }
  $form['language'] = array(
    '#type' => 'select',
    '#title' => 'Language',
    '#options' => $language_options,
    '#required' => FALSE,
    '#default_value' => $record['id'] ?
      $record['language'] :
      '',
  );
  $bundle_info = field_info_bundles();
  $options = array();
  foreach ($bundle_info as $entity_type => $bundles) {
    $entity_types[] = $entity_type;
    foreach ($bundles as $bundle => $info) {
      $options[$entity_type][$entity_type . ':' . $bundle] = $info['label'];
    }
    asort($options[$entity_type]);
  }
  $form['bundle'] = array(
    '#type' => 'select',
    '#title' => 'Bundle',
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => $record['id'] ?
      $record['entity_type'] . ':' . $record['bundle'] :
      NULL,
  );

  $form['pattern'] = array(
    '#type' => 'textfield',
    '#title' => 'Pattern',
    '#required' => TRUE,
    '#default_value' => $record['pattern'],
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => $record['id'] ? t('Update pattern') : t('Add pattern'),
  );

  if (empty($record['id'])) {
    $rows = array();
    $result = db_select('pathauto_paths', 'pp')
      ->fields('pp')
      ->execute();

    foreach ($result as $row) {
      $operations = array();
      $operations[] = l('edit', 'admin/config/search/path/path/' . $row->id);
      $operations[] = l('delete', "admin/config/search/path/path/{$row->id}/delete");
      $row->operations = implode(' ', $operations);
      $rows[] = (array) $row;
    }
    $form['patterns'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array(
        'id',
        'Router path',
        'Entity type',
        'Bundle',
        'Pattern',
        'Language',
        'Operations',
      ),
      '#weight' => 1000,
    );
  }

  return $form;
}

/**
 * Submit callback for Pathauto paths settings form.
 */
function pathauto_paths_settings_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  list ($entity_type, $bundle) = explode(':', $values['bundle']);
  $record = array(
    'router_path' => $values['router_path'],
    'entity_type' => $entity_type,
    'bundle' => $bundle,
    'pattern' => $values['pattern'],
    'language' => $values['language'],
  );
  $keys = array();
  if ($form_state['pattern_id']) {
    $keys = array('id');
    $record['id'] = $form_state['pattern_id'];
    
  }
  if (drupal_write_record('pathauto_paths', $record, $keys) == SAVED_NEW) {
    drupal_set_message('The pattern was created.');
  }
  else {
    drupal_set_message('The pattern was updated.');
    $form_state['redirect'] = 'admin/config/search/path/path';
  }
}

/**
 * Delete confirm form.
 */
function pathauto_paths_delete_confirm_form($form, $form_state, $record) {
  $form['pattern_id'] = array(
    '#type' => 'value',
    '#value' => $record['id'],
  );
  $title = t('Are you sure you want to delete the pattern for %path ?', array('%path' => $record['router_path']));
  $description = t('Router path: %path', array('%path' => $record['router_path'])) . '<br />';
  $description .= t('Entity type: %type', array('%type' => $record['entity_type'])) . '<br />';
  $description .= t('Bundle: %bundle', array('%bundle' => $record['bundle'])) . '<br />';
  $description .= t('Pattern: %pattern', array('%pattern' => $record['pattern'])) . '<br />';
  $description .= t('Language: %language', array('%language' => $record['language'])) . '<br />';

  return confirm_form($form,
    $title,
    'admin/config/search/path/path',
    $description);
}

/**
 * Submit handler for pathauto paths delete confirm form.
 */
function pathauto_paths_delete_confirm_form_submit($form, &$form_state) {
  db_delete('pathauto_paths')
    ->condition('id', $form_state['values']['pattern_id'])
    ->execute();
  drupal_set_message('The record was deleted.');
  $form_state['redirect'] = 'admin/config/search/path/path';
}